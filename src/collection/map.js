function doubleItem(collection) {
  // Need to be implemented
  return collection.map(element => element * 2);
}

function doubleEvenItem(collection) {
  // Need to be implemented
  return collection.filter(element => element % 2 === 0).map(element => element * 2);
}

function covertToCharArray(collection) {
  // Need to be implemented
  return collection.map(element => String.fromCharCode(96 + element));
}

function getOneClassScoreByASC(collection) {
  // Need to be implemented
  return collection.filter(element => element.class === 1).map(element => element.score).sort((left, right) => left - right);
}

export { doubleItem, doubleEvenItem, covertToCharArray, getOneClassScoreByASC };
