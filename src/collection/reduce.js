function getMaxNumber(collction) {
  // Need to be implemented
  return collction.reduce((accumulator, currentValue) => Math.max(accumulator, currentValue));
}

function isSameCollection(collction1, collction2) {
  // Need to be implemented
  if (collction1.length !== collction2.length) {
    return false;
  }

  return collction1.reduce((result, currentValue, index) => {
    if (currentValue === collction2[index]) {
      return result;
    } else {
      return false;
    }
  });
}

function sum(collction) {
  // Need to be implemented
  return collction.reduce((accumulator, currentValue) => accumulator + currentValue);
}

function computeAverage(collction) {
  // Need to be implemented
  return collction.reduce((accumulator, currentValue) => accumulator + currentValue) / collction.length;
}

function lastEven(collction) {
  // Need to be implemented
  const evenArray = collction.filter(element => element % 2 === 0);
  return evenArray.reduce((accumulator, currentValue) => {
    if (evenArray.indexOf(accumulator) < evenArray.indexOf(currentValue)) {
      return currentValue;
    }
  });
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
