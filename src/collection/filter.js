function getAllEvens(collection) {
  // Need to be implemented
  return collection.filter(element => element % 2 === 0);
}

function getAllIncrementEvens(start, end) {
  // Need to be implemented
  const array = [];
  for (let i = 1; i < end - start + 2; i++) {
    array.push(i);
  }
  return array.filter(element => element % 2 === 0);
}

function getIntersectionOfcollections(collection1, collection2) {
  // Need to be implemented
  return collection1.filter(element => collection2.includes(element));
}

function getUnionOfcollections(collection1, collection2) {
  // Need to be implemented
  return collection1.concat(collection2.filter(element => !collection1.includes(element)));
}

function countItems(collection) {
  // Need to be implemented
  const result = {};
  collection.forEach(index => { result[index] = collection.filter(element => element === index).length; });
  return result;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
