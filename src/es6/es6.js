function flatArray(arr) {
  // Need to be implemented
  return arr.flat();
}

function aggregateArray(arr) {
  // Need to be implemented
  return arr.map(element => [element * 2]);
}

function getEnumerableProperties(obj) {
  // Need to be implemented
  return Object.keys(obj);
}

function removeDuplicateItems(arr) {
  // Need to be implemented
  return [...new Set(arr)];
}

function removeDuplicateChar(str) {
  // Need to be implemented
  return [...new Set([...str])].join('');
}

function addItemToSet(set, item) {
  // Need to be implemented
  return set.add(item);
}

function removeItemToSet(set, item) {
  // Need to be implemented
  set.delete(item);
  return set;
}

function countItems(arr) {
  // Need to be implemented
  const map = new Map();
  const setArray = [...new Set(arr)];
  for (let i = 0; i < setArray.length; i++) {
    map.set(setArray[i], arr.filter(element => element === setArray[i]).length);
  }
  return map;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
